#!/bin/sh
set -e

if [ -z "$TERM" ]; then
    export TERM=dumb
fi

set -u

if [ "$TERM" != dumb ] && command -v tput > /dev/null; then
    YELLOW="$(tput setaf 3)"
    RESET="$(tput sgr0)"
else
    YELLOW=""
    RESET=""
fi

run() {
    echo "${YELLOW}\$ $@${RESET}"
    "$@"
    echo
}

SWITCH="$(basename "$0")"
export HOME="$(mktemp -d)"
trap 'rm -rf -- "$HOME"' EXIT

run opam init --disable-sandbox --no-setup --compiler="$SWITCH"
run opam install -y merlin ocp-indent utop
